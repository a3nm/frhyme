#!/bin/bash

NUM=${1:?Usage: $0 NUM_PHONEMES}
awk  --field-separator="\t" '{ printf "%s\t%s\n", $1, substr( $2, length($2) - '$NUM' + 1) }'

