import setuptools

with open("README", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='frhyme',
    version='0.2',
    author="Antoine Amarilli",
    author_email="a3nm@a3nm.net",
    package_data={'frhyme' :['*json']},
    description="Guess the last phonemes of a French word",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/a3nm/frhyme",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
)
